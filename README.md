6S battery breakout
===================

A small breakout PCB that will convert six 1S batteries to one big 6S battery so you can parallel-charge them with your
good quality, big charger.
