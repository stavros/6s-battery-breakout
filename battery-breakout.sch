EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:battery-breakout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Battery Balance Board"
Date "2018-11-24"
Rev "1"
Comp "Stochastic Technologies"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x02_Male J3
U 1 1 5BF898A9
P 4100 3750
F 0 "J3" H 4100 3850 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4100 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.00mm" H 4100 3750 50  0001 C CNN
F 3 "" H 4100 3750 50  0001 C CNN
	1    4100 3750
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Male J5
U 1 1 5BF898ED
P 4100 4550
F 0 "J5" H 4100 4650 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4100 4350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.00mm" H 4100 4550 50  0001 C CNN
F 3 "" H 4100 4550 50  0001 C CNN
	1    4100 4550
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Male J4
U 1 1 5BF89925
P 4100 4150
F 0 "J4" H 4100 4250 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4100 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.00mm" H 4100 4150 50  0001 C CNN
F 3 "" H 4100 4150 50  0001 C CNN
	1    4100 4150
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Male J1
U 1 1 5BF89959
P 4100 2950
F 0 "J1" H 4100 3050 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4100 2750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.00mm" H 4100 2950 50  0001 C CNN
F 3 "" H 4100 2950 50  0001 C CNN
	1    4100 2950
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Male J2
U 1 1 5BF89988
P 4100 3350
F 0 "J2" H 4100 3450 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4100 3150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.00mm" H 4100 3350 50  0001 C CNN
F 3 "" H 4100 3350 50  0001 C CNN
	1    4100 3350
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Female J7
U 1 1 5BF89CE9
P 5850 3050
F 0 "J7" H 5850 3150 50  0000 C CNN
F 1 "Conn_01x02_Female" H 5850 2850 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x02_Pitch2.54mm" H 5850 3050 50  0001 C CNN
F 3 "" H 5850 3050 50  0001 C CNN
	1    5850 3050
	0    -1   -1   0   
$EndComp
$Comp
L Conn_01x02_Male J6
U 1 1 5BF8983D
P 4100 4950
F 0 "J6" H 4100 5050 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4100 4750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.00mm" H 4100 4950 50  0001 C CNN
F 3 "" H 4100 4950 50  0001 C CNN
	1    4100 4950
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x07_Female J8
U 1 1 5BF8AC88
P 6300 4000
F 0 "J8" H 6300 4400 50  0000 C CNN
F 1 "Conn_01x07_Female" H 6300 3600 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x07_Pitch2.54mm" H 6300 4000 50  0001 C CNN
F 3 "" H 6300 4000 50  0001 C CNN
	1    6300 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5BF8B15E
P 5800 3500
F 0 "#PWR01" H 5800 3250 50  0001 C CNN
F 1 "GND" H 5800 3350 50  0000 C CNN
F 2 "" H 5800 3500 50  0001 C CNN
F 3 "" H 5800 3500 50  0001 C CNN
	1    5800 3500
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR02
U 1 1 5BF8B240
P 6000 3500
F 0 "#PWR02" H 6000 3350 50  0001 C CNN
F 1 "VCC" H 6000 3650 50  0000 C CNN
F 2 "" H 6000 3500 50  0001 C CNN
F 3 "" H 6000 3500 50  0001 C CNN
	1    6000 3500
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR03
U 1 1 5BF8B392
P 7550 3150
F 0 "#PWR03" H 7550 3000 50  0001 C CNN
F 1 "VCC" H 7550 3300 50  0000 C CNN
F 2 "" H 7550 3150 50  0001 C CNN
F 3 "" H 7550 3150 50  0001 C CNN
	1    7550 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5BF8B3B6
P 7850 3250
F 0 "#PWR04" H 7850 3000 50  0001 C CNN
F 1 "GND" H 7850 3100 50  0000 C CNN
F 2 "" H 7850 3250 50  0001 C CNN
F 3 "" H 7850 3250 50  0001 C CNN
	1    7850 3250
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG05
U 1 1 5BF8B3DA
P 7850 3150
F 0 "#FLG05" H 7850 3225 50  0001 C CNN
F 1 "PWR_FLAG" H 7850 3300 50  0000 C CNN
F 2 "" H 7850 3150 50  0001 C CNN
F 3 "" H 7850 3150 50  0001 C CNN
	1    7850 3150
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG06
U 1 1 5BF8B420
P 7550 3250
F 0 "#FLG06" H 7550 3325 50  0001 C CNN
F 1 "PWR_FLAG" H 7550 3400 50  0000 C CNN
F 2 "" H 7550 3250 50  0001 C CNN
F 3 "" H 7550 3250 50  0001 C CNN
	1    7550 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 2950 5550 2950
Wire Wire Line
	5550 2950 5550 3700
Wire Wire Line
	5550 3700 6100 3700
Wire Wire Line
	4300 3050 4600 3050
Wire Wire Line
	4600 3050 4600 3350
Wire Wire Line
	4600 3350 4300 3350
Wire Wire Line
	4300 3450 4600 3450
Wire Wire Line
	4600 3450 4600 3750
Wire Wire Line
	4600 3750 4300 3750
Wire Wire Line
	4300 3850 4600 3850
Wire Wire Line
	4600 3850 4600 4150
Wire Wire Line
	4600 4150 4300 4150
Wire Wire Line
	4300 4250 4600 4250
Wire Wire Line
	4600 4250 4600 4550
Wire Wire Line
	4600 4550 4300 4550
Wire Wire Line
	4300 4650 4600 4650
Wire Wire Line
	4600 4650 4600 4950
Wire Wire Line
	4600 4950 4300 4950
Wire Wire Line
	4300 5050 5550 5050
Wire Wire Line
	5550 5050 5550 4300
Wire Wire Line
	4600 3200 5450 3200
Wire Wire Line
	5450 3200 5450 3800
Wire Wire Line
	5450 3800 6100 3800
Connection ~ 4600 3200
Wire Wire Line
	4600 3600 5350 3600
Wire Wire Line
	5350 3600 5350 3900
Wire Wire Line
	5350 3900 6100 3900
Connection ~ 4600 3600
Wire Wire Line
	4600 4000 6100 4000
Connection ~ 4600 4000
Wire Wire Line
	5550 4300 6100 4300
Wire Wire Line
	4600 4400 5350 4400
Wire Wire Line
	5350 4400 5350 4100
Wire Wire Line
	5350 4100 6100 4100
Connection ~ 4600 4400
Wire Wire Line
	6100 4200 5450 4200
Wire Wire Line
	5450 4200 5450 4800
Wire Wire Line
	5450 4800 4600 4800
Connection ~ 4600 4800
Wire Wire Line
	7550 3150 7550 3250
Wire Wire Line
	7850 3150 7850 3250
Wire Wire Line
	5850 3250 5850 3700
Connection ~ 5850 3700
Wire Wire Line
	5950 3250 5950 4300
Connection ~ 5950 4300
Wire Wire Line
	5800 3500 5850 3500
Connection ~ 5850 3500
Wire Wire Line
	5950 3500 6000 3500
Connection ~ 5950 3500
$EndSCHEMATC
